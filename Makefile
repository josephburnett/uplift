.PHONY : build
build :
	KO_DOCKER_REPO=us-central1-docker.pkg.dev/jburnett-ad8e5d54/uplift ko build .

.PHONY : down
down :
	gcloud compute instance-groups managed resize uplift-manager-gcp --size=0
	gcloud compute instance-groups managed resize uplift-worker      --size=0
