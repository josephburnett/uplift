package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/xanzy/go-gitlab"
	compute "google.golang.org/api/compute/v1"
)

func main() {
	uplift, err := New()
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/jobs", uplift.jobsHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

type Uplift struct {
	gitlabClient  *gitlab.Client
	computeClient *compute.Service
	config        Config
}

type Config struct {
	Project string
	Zone    string
	Runners []RunnerConfig
}

type RunnerConfig struct {
	Tags []string
	Name string
}

func New() (*Uplift, error) {
	token := os.Getenv("GL_TOKEN")
	gitlabClient, err := gitlab.NewClient(token)
	if err != nil {
		return nil, err
	}
	computeClient, err := compute.NewService(context.Background())
	if err != nil {
		return nil, err
	}

	return &Uplift{
		gitlabClient:  gitlabClient,
		computeClient: computeClient,
		config: Config{
			Project: "jburnett-ad8e5d54",
			Zone:    "us-central1-a",
			Runners: []RunnerConfig{{
				Tags: []string{"gcp"},
				Name: "uplift-manager-gcp",
			}, {
				Tags: []string{"aws"},
				Name: "uplift-manager-aws",
			}},
		},
	}, nil
}

type JobEvent struct {
	ID        int64  `json:"build_id"`
	ProjectID int64  `json:"project_id"`
	Created   string `json:"build_created_at"`
	Runner    Runner `json:"runner"`
}

type Runner struct {
	Active      bool     `json:"active"`
	RunnerType  string   `json:"runner_type"`
	IsShared    bool     `json:"is_shared"`
	ID          int64    `json:"id"`
	Description string   `json:"description"`
	Tags        []string `json:"tags"`
}

func (u *Uplift) jobsHandler(w http.ResponseWriter, r *http.Request) {

	// Read the JobEvent
	eventType := r.Header.Get("X-GitLab-Event")
	log.Printf("got event type %v", eventType)
	if eventType != "Job Hook" {
		log.Printf("ignoring event type: %v", eventType)
		return
	}
	jobEvent := &JobEvent{}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("error reading body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = json.Unmarshal(body, jobEvent)
	if err != nil {
		log.Printf("error unmarshaling job: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Println(string(body))

	// Activate the necessary Runners
	job, _, err := u.gitlabClient.Jobs.GetJob(int(jobEvent.ProjectID), int(jobEvent.ID))
	if err != nil {
		log.Printf("error getting job: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	runners := u.runnersActivatedByTags(job.TagList)
	for _, r := range runners {
		err = u.activateRunner(r)
		if err != nil {
			log.Printf("error activating runner: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

func (u *Uplift) runnersActivatedByTags(tags []string) []string {
	runners := []string{}
	for _, r := range u.config.Runners {
		hasAllTags := true
		for _, t := range tags {
			hasTag := false
			for _, rt := range r.Tags {
				if t == rt {
					hasTag = true
				}
			}
			if !hasTag {
				hasAllTags = false
			}
		}
		if hasAllTags {
			runners = append(runners, r.Name)
		}
	}
	log.Printf("runners %v are activated by tags %v", runners, tags)
	return runners
}

func (u *Uplift) activateRunner(name string) error {
	ig, err := u.computeClient.InstanceGroupManagers.Get(u.config.Project, u.config.Zone, name).Do()
	if err != nil {
		return err
	}
	if ig.TargetSize > 0 {
		log.Printf("runner %v is already activated", name)
		return nil
	}
	_, err = u.computeClient.InstanceGroupManagers.Resize(u.config.Project, u.config.Zone, name, 1).Do()
	log.Printf("activated runner %v", name)
	return err
}
