# uplift

Uplift is a proof-of-concept for a serverless GitLab Runner deployment.

**Demo: https://youtu.be/W70h9xI2UIs**

![diagram](diagram.png){width=50%}

## Getting started

1. Create an instance group for each runner manager scaling from 0 to 1.
2. Create an instance group of workers for each runner manager scaling from 0 to N.
3. [Configure](https://gitlab.com/josephburnett/uplift/-/blob/main/main.go#L56-66) uplift with the tags for each runner.
4. Run `make` to build and upload the uplift image.
5. Create a Cloud Run function with the uplift image.
6. Point your project Job Event webhooks at the Cloud Run function.
7. (implmement an idler component to bring down the managers after a period without jobs)

## Support

This project has no support. It's just a working proof-of-concept.

## License

MIT

## Project status

Works.